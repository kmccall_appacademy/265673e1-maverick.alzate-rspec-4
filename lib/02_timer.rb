class Timer
  @seconds

  attr_accessor :seconds

  def initialize
    @seconds = 0
  end

  def time_string
    # get hours and minutes if available
    time = second_converter
    # format
    time.map! { |num| padded(num) }.join(":")
    # return
  end

  def second_converter
    remainder = @seconds
    hours = 0
    minutes = 0
    # Check for hours
    if remainder > 3600
      hours = remainder / 3600
      remainder -= hours * 3600
    end
    # Check for minutes
    if remainder > 60
      minutes = remainder / 60
      remainder -= minutes * 60
    end
    # return hours, minutes, and remaining seconds
    [hours, minutes, remainder]
  end

  def padded(num)
    padded_num = num.to_s
    padded_num.length < 2 ? "0#{padded_num}" : padded_num
  end

end
