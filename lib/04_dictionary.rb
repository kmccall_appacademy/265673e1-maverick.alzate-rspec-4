class Dictionary
  @entries

  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(hash)
    # no definition
    if hash.class != Hash
      @entries[hash] = nil
    # with definition
    else
      @entries[hash.keys[0]] = hash.values[0]
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(keyword)
    true if @entries.key?(keyword)
  end

  def find(substring)
    @entries.select { |keyword, defi| keyword.include?(substring) }
  end

  def printable
    prints = @entries.map do |keyword, defi|
      "[#{keyword}] \"#{defi}\""
    end
    prints.sort.join("\n")
  end
end
