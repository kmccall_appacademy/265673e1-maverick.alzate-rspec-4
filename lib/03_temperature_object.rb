class Temperature
  @temp = {}

  # takes option hash
  def initialize(option)
    @temp = {}
    @temp[option.keys[0]] = option.values[0]
  end

  def self.from_fahrenheit(num)
    Temperature.new(f: num)
  end

  def self.from_celsius(num)
    Temperature.new(c: num)
  end

  def in_fahrenheit
    return @temp[:f] if @temp.keys[0] == :f
    @temp[:c] * 9.0 / 5.0 + 32.0
  end

  def in_celsius
    return @temp[:c] if @temp.keys[0] == :c
    (@temp[:f] - 32.0) * 5.0 / 9.0
  end
end

class Celsius < Temperature

  def initialize(num)
    @temp = num
  end

  def in_fahrenheit
    @temp * 9.0 / 5.0 + 32.0
  end

  def in_celsius
    @temp
  end
end

class Fahrenheit < Temperature
  def initialize(num)
    @temp = num
  end

  def in_fahrenheit
    @temp
  end

  def in_celsius
    (@temp - 32.0) * 5.0 / 9.0
  end
end
