class Book

  @title = ""

  CONJUNCTIONS = ["and", "or", "but"].freeze
  PREPOSITIONS = ["in", "of", "over", "under"].freeze
  ARTICLES = ["the", "a", "an"].freeze

  def initialize; end

  def title=(string)
    words = string.split
    # capitalize first word
    words[0].capitalize!
    # capitalize rest of words according to rules
    if words.length > 1
      (1...words.length).each do |index|
        if !CONJUNCTIONS.include?(words[index]) &&
           !PREPOSITIONS.include?(words[index]) &&
           !ARTICLES.include?(words[index])
          words[index].capitalize!
          puts words[index]
        end
      end
    end
    # join words and return
    @title = words.join(" ")
  end

  def title
    @title
  end


end
